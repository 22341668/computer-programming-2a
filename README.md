# COMPUTER PROGRAMMING 2A

## Introduction

The objective of this module is to introduce this rapidly growing field. In this module you will acquire the basic programming skills which will be applied in future modules within your branch of engineering.

## Prerequisites

All students are expected to be proficient in the following knowledge:
- Typing and the computer QWERTY keyboard layout.
- Complete working knowledge of the Windows operating system.
- Working with Excel for importing and analysis of data.
- The hardware associated with computer systems.
- Understanding the concepts of interrupts as used in the digital systems field.
- Understanding how memory allocation takes place in a computer system and how memory is used 
to store digital data.
- Basic algebra, trigonometry, relational mathematical operators and Boolean algebra.
- Graphical representation and analysis of numerical data.
- The hardware governing serial communications.
- Basic ac and dc circuit theory:
    - Resistors, capacitors, inductors in series and parallel.
    - Voltage, current and power relations in circuits.
