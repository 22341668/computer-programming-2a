"""
Exercise 1:
-------------------------------------------------------------------------------
1. Declare three variables `a`, `b`, and `c`, and assign them integer values of
your choice.
2. Print the values of `a`, `b`, and `c` separately.
3. Reassign the values of `a`, `b`, and `c` to different values (e.g., strings,
floats, or booleans).
4. Print the new values of `a`, `b`, and `c`.
5. Perform a mathematical operation using the variables (e.g., addition, 
subtraction, multiplication, 
or division), and store the result in a new variable `result`.
6. Print the value of `result`.
7. Assign the same value (e.g., 10) to all three variables `x`, `y`, and `z` in
a single line.
8. Print the values of `x`, `y`, and `z` separately.
"""

# code
 