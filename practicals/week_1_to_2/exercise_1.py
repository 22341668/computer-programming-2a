"""
Exercise 1:
Calculate the multiplication and sum of two numbers
-------------------------------------------------------------------------------

Given two integer numbers, return their product only if the product is equal
to or lower than 1000. Otherwise, return their sum.
"""


def sum_or_product(int1, int2):
    calc_res = int1 * int2

    if calc_res <= 1000:
        return calc_res

    else:
        return int1 + int2


var1 = 3
var2 = 5

var3 = 400
var4 = 3

print(f"sum_or_product({var1}, {var2})")
print("->", sum_or_product(var1, var2))

print(f"sum_or_product({var3}, {var4})")
print("->", sum_or_product(var3, var4))


"""
> python3 ./practicals/week_1/exercise_1.py

sum_or_product(3, 5)
-> 15
sum_or_product(400, 3)
-> 403
"""
