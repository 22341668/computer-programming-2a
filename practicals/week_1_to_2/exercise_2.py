"""
Exercise 2:
Print the sum of the current number and the previous number
-------------------------------------------------------------------------------

Write a program to iterate the first 10 numbers, and in each iteration, print
the sum of the current and previous number.
"""

my_list = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

for index in range(1, 10):
    res = my_list[index] + my_list[index - 1]
    print(my_list[index], "+", my_list[index - 1], "=",
          res)

"""
> python3 ./practicals/week_1/exercise_2.py

4 + 2 = 6
6 + 4 = 10
8 + 6 = 14
10 + 8 = 18
12 + 10 = 22
14 + 12 = 26
16 + 14 = 30
18 + 16 = 34
20 + 18 = 38
"""
