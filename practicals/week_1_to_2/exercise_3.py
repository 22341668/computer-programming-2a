"""
Exercise 3:
Print characters from a string that are present at an even index number
-------------------------------------------------------------------------------

Write a program to accept a string from the user and display characters that
are present at an even index number.
"""


def even_chars(string):
    for index, char in enumerate(string):
        if (index % 2) == 0:
            print(char)


even_chars("sipho")

"""
> python3 ./practicals/week_1/exercise_3.py

s
p
o
"""
