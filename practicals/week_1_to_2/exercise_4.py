"""
Exercise 4:
Remove first n characters from a string
-------------------------------------------------------------------------------

Write a program to remove characters from a string starting from zero up to n
and return a new string.
"""


def remove_zero_upto_n(string, n):
    return string[n:]


string = "mokoena"
print(string)

res = remove_zero_upto_n(string, 2)
print(res)

"""
> python3 ./practicals/week_1/exercise_4.py

mokoena
koena
"""
