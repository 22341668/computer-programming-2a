"""
Question 1: Calculator Program
-------------------------------------------------------------------------------

Write a Python program for a simple calculator that can perform addition,
subtraction, multiplication, and division. Your program should take two numbers
and an operator as input from the user and display the result. Additionally,
ensure that the program handles invalid inputs gracefully and provides
appropriate error messages.

Requirements:
1. Use variables to store the two numbers and the result.
2. Use appropriate data types for the variables.
3. Implement functions for addition, subtraction, multiplication, and division.
4. Use conditional statements to determine which operation to perform.
5. Include error handling for invalid inputs, such as non-numeric values or
division by zero
"""

import operator

ops = {
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.truediv
}


def calculator(var1, var2, operator):
    """
    Simple calculator function

    Args:
        var1 (int): First number to operate on
        var2 (int): Second number to operate on
        operator (str): Should be "+" or "-" or "*" or "/"

    Returns:
        _sum: int | float | None
    """
    if not isinstance(var1, int):
        print(f"[ERROR] [CRITICAL] : var1 of {type(var1)} is not an integer!")

    elif not isinstance(var2, int):
        print(f"[ERROR] [CRITICAL] : var2 of {type(var2)} is not an integer!")

    try:
        _sum = ops[operator](var1, var2)

    except ZeroDivisionError:
        print("[ERROR] [CRITICAL] : You cannot divide by 0")
        return None

    return _sum


# Tests
# -- Addition
print(calculator(2, 3, "+"))  # 5

# -- Subtraction
print(calculator(5, 3, "-"))  # 2

# -- Multiplication
print(calculator(2, 3, "*"))  # 6

# -- Division
print(calculator(4, 2, "/"))  # 2.0

# -- Catching division by Zero
print(calculator(2, 0, "/"))  # None
